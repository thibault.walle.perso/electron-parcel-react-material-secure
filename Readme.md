# Electron boilerplate

* [Parcel Bundler](https://parceljs.org/) a zero conf alternative to webpack
* [Electron](https://www.electronjs.org/) create desktop apps with web technologies
* [React](https://reactjs.org/) front-end lib
* [Material UI](https://material-ui.com/) UI lib, similar to android
* [Husky](https://github.com/typicode/husky) Git hooks
* [Eslint](https://eslint.org/) Code linter

# Installation

##### Linux only, electron js requirements

```bash
sudo apt install -y libgbm-dev
sudo apt install libnss3-dev
```

##### Everyone

```bash
npm install
npm run dev
```

# Security focused

1.  ✅ [Only load secure content](https://electronjs.org/docs/tutorial/security#1-only-load-secure-content)
2.  ✅ [Do not enable node.js integration for remote content](https://electronjs.org/docs/tutorial/security#2-do-not-enable-nodejs-integration-for-remote-content)
3.  ✅ [Enable context isolation for remote content](https://electronjs.org/docs/tutorial/security#3-enable-context-isolation-for-remote-content)
4.  ✅ [Do not disable websecurity](https://electronjs.org/docs/tutorial/security#5-do-not-disable-websecurity)
5.  ✅ [Define a content security policy](https://electronjs.org/docs/tutorial/security#6-define-a-content-security-policy)
6.  ✅ [Do not set allowRunningInsecureContent to true](https://electronjs.org/docs/tutorial/security#7-do-not-set-allowrunninginsecurecontent-to-true)
7.  ✅ [Do not enable experimental features](https://electronjs.org/docs/tutorial/security#8-do-not-enable-experimental-features)
8.  ✅ [Do not use enableBlinkFeatures](https://electronjs.org/docs/tutorial/security#9-do-not-use-enableblinkfeatures)
9.  ✅ [Use a current version of electron](https://electronjs.org/docs/tutorial/security#17-use-a-current-version-of-electron)
10. ✅ [Handle session permission requests from remote content](https://electronjs.org/docs/tutorial/security#4-handle-session-permission-requests-from-remote-content)
11. ✅ [Disable or limit navigation](https://electronjs.org/docs/tutorial/security#12-disable-or-limit-navigation)
12. ✅ [&lt;webview&gt; verify options and params](https://electronjs.org/docs/tutorial/security#11-verify-webview-options-before-creation)
13. ✅ [Disable or limit creation of new windows](https://electronjs.org/docs/tutorial/security#13-disable-or-limit-creation-of-new-windows)
14. ✅ [Do not use openExternal with untrusted content](https://electronjs.org/docs/tutorial/security#14-do-not-use-openexternal-with-untrusted-content)
15. ✅ [Disable remote module](https://electronjs.org/docs/tutorial/security#15-disable-the-remote-module)
16. ✅ [Filter the remote module](https://electronjs.org/docs/tutorial/security#16-filter-the-remote-module)

# IPC as HTTP

Good security practice is to use IPC to Communicate asynchronously from the main process to renderer processes. I used HTTP server instead, because there is more front-end tooling, great developer experience with chrome devtools, status code and ability to live reload the http server independently of electron process

# Git hooks (pre-push)

* **mandatory** Run eslint, if not correct pre-push fails
* **info only** npm security audit
* **info only** npm packages possible upgrades

You can tweak hooks in `package.json`
