const { contextBridge } = require("electron");

// Expose protected methods that allow the renderer process to use
// the ipcRenderer without exposing the entire object
contextBridge.exposeInMainWorld(
  "env", {
    port: +process.argv.find(a => a.includes('port:')).replace('port:', '') // Find first available port found in "main.js"
  }
);
