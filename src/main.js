const { app, BrowserWindow, session } = require('electron');
const express = require('express');
const getPort = require('get-port');
const router = require('./http/routes');

const server = express();

const createWindowContext = (port) => {
  const win = new BrowserWindow({
    width: 1024,
    height: 768,
    resizable: false,
    autoHideMenuBar: true,
    show: false,
    webPreferences: {
      nodeIntegration: false,
      contextIsolation: true,
      sandbox: true,
      webSecurity: true,
      allowDisplayingInsecureContent: false,
      allowRunningInsecureContent: false,
      nodeIntegrationInWorker: false,
      nodeIntegrationInSubFrames: false,
      enableRemoteModule: false,
      additionalArguments: [`port:${port}`],
      preload: `${__dirname}/preload.js`
    }
  });

  win.loadFile(`${__dirname}/dist/index.html`);

  win
    .webContents
    .on('did-finish-load',  () => {
      win.show();
    });
};

const createWindow = async () => {

  /**
   * We want a separate process in dev mode to enable http server reload on change
   */
  if (process.env.NODE_ENV !== 'development') {
    const PORT = await getPort(); // Find free TCP port
    server.use('/', router);
    server.listen(PORT, () => {
      console.log(`Server listening at http://localhost:${PORT}`);
      createWindowContext(PORT);
    });
  } else {
    createWindowContext(3000);
  }

  session
    .fromPartition("default")
    .setPermissionRequestHandler((webContents, permission, permCallback) => {
      const allowedPermissions = []; // Full list here: https://developer.chrome.com/extensions/declare_permissions#manifest

      if (allowedPermissions.includes(permission)) {
        permCallback(true); // Approve permission request
      } else {
        console.error(
          `The application tried to request permission for '${permission}'. This permission was not whitelisted and has been blocked.`
        );

        permCallback(false); // Deny
      }
    });

  // Filter loading any module via remote;
  // you shouldn't be using remote at all, though
  // https://electronjs.org/docs/tutorial/security#16-filter-the-remote-module
  app.on("remote-require", (event) => {
    event.preventDefault();
  });
};

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

app.on("web-contents-created", (event, contents) => {
  // https://electronjs.org/docs/tutorial/security#12-disable-or-limit-navigation
  contents.on("will-navigate", (contentsEvent, navigationUrl) => {
    const parsedUrl = new URL(navigationUrl);
    const validOrigins = 'localhost';

    // Log and prevent the app from navigating to a new page if that page's origin is not whitelisted
    if (!validOrigins.includes(parsedUrl.origin)) {
      console.error(
        `The application tried to redirect to the following address: '${parsedUrl}'. This origin is not whitelisted and the attempt to navigate was blocked.`
      );

      contentsEvent.preventDefault();
      return false;
    }
  });

  contents.on("will-redirect", (contentsEvent, navigationUrl) => {
    const parsedUrl = new URL(navigationUrl);
    const validOrigins = [];

    // Log and prevent the app from redirecting to a new page
    if (!validOrigins.includes(parsedUrl.origin)) {
      console.error(
        `The application tried to redirect to the following address: '${navigationUrl}'. This attempt was blocked.`
      );

      contentsEvent.preventDefault();
      return false;
    }
  });

  // https://electronjs.org/docs/tutorial/security#11-verify-webview-options-before-creation
  contents.on("will-attach-webview", (contentsEvent, webPreferences) => {
    // Strip away preload scripts if unused or verify their location is legitimate
    delete webPreferences.preload;
    delete webPreferences.preloadURL;

    // Disable Node.js integration
    webPreferences.nodeIntegration = false;
  });

  // https://electronjs.org/docs/tutorial/security#13-disable-or-limit-creation-of-new-windows
  contents.on("new-window", async (contentsEvent, navigationUrl) => {
    const parsedUrl = new URL(navigationUrl);
    const validOrigins = [];

    // Log and prevent opening up a new window
    if (!validOrigins.includes(parsedUrl.origin)) {
      console.error(
        `The application tried to open a new window at the following address: '${navigationUrl}'. This attempt was blocked.`
      );

      contentsEvent.preventDefault();
      return false;
    }
  });
});
