/**
 * This file is designed to be used only in development to enable http live reload on change with nodemon
 * Production application config is located in main.js
 */
const express = require('express');
const router = require('./http/routes');

const server = express();
const port = 3000;

server.use('/', router);
server.listen(port, () => console.log(`Server listening at http://localhost:${port}`));
