const express = require('express');
const router = express.Router();

router.get('/', require('./controller/HomeController'));

module.exports = router;
