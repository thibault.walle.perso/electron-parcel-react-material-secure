const HomeRoute = (req, res) => {
  return res.json({
    os: process.platform
  });
};

module.exports = HomeRoute;
