import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { Button, Container, createMuiTheme, CssBaseline, Grid } from "@material-ui/core";
import { ThemeProvider } from '@material-ui/core/styles';

const darkTheme = createMuiTheme({
  palette: {
    type: 'dark',
  },
});

const App = () => {
  const [os, setOs] = useState(null);

  useEffect(() => {
    fetch(`http://localhost:${window.env.port}`)
      .then(r => r.json())
      .then(r => setOs(r.os));
  }, []);

  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline>
        <Container>
          <Grid container>
            <Grid item xs={12}>
              <Button variant="contained">Your OS is {os}</Button>
            </Grid>
          </Grid>
        </Container>
      </CssBaseline>
    </ThemeProvider>
  );
};

ReactDOM.render(
  <App />,
  document.querySelector("#app"),
);
